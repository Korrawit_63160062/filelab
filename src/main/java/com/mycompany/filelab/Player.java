/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.filelab;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class Player implements Serializable {

    private char symbol;
    private int win, loss, draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        this.win++;
    }

    public int getLoss() {
        return loss;
    }

    public void loss() {
        this.loss++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        this.draw++;
    }
    
    public void clearStat() {
        this.win = 0;
        this.draw = 0;
        this.loss = 0;
    }

    @Override
    public String toString() {
        return "Player " + symbol + " Win : " + win + ", Loss : " + loss + ", Draw : " + draw;
    }

}
